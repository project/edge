<?php

/**
 * @file
 * Cutting Edge functionality.
 */

/**
 * Implements hook_element_info().
 */
function edge_element_info() {
  $types['table'] = array(
    '#header' => array(),
    '#rows' => array(),
    '#empty' => '',
    '#pre_render' => array('edge_pre_render_table'),
    '#theme' => 'table',
    // Properties for tableselect support.
    '#tableselect' => FALSE,
    '#input' => TRUE,
    '#value_callback' => 'edge_type_table_value',
    '#tree' => TRUE,
    // Note that MODULE_process_THEME() clashes with global theme system process
    // callbacks.
    '#process' => array('edge_element_process_table'),
    '#element_validate' => array('edge_element_validate_table'),
    '#multiple' => TRUE,
    '#js_select' => TRUE,
  );
  return $types;
}

/**
 * Implements hook_theme_registry_alter().
 */
function edge_theme_registry_alter(&$callbacks) {
  $path = drupal_get_path('module', 'edge');

  // Apply replacement theme callbacks, but only if they have not been
  // overridden.
  $edge_theme = array('item_list', 'links');
  foreach ($edge_theme as $callback) {
    if (isset($callbacks[$callback]) && $callbacks[$callback]['function'] == 'theme_' . $callback) {
      $callbacks[$callback]['function'] = 'theme_edge_' . $callback;
      $callbacks[$callback]['includes'][] = $path . '/edge.theme.inc';
      $callbacks[$callback]['file'] = 'edge.theme.inc';

      // Perform additional callback-specific tweaks, if necessary.
      switch ($callback) {
        case 'item_list':
          $callbacks[$callback]['variables']['title'] = '';
          break;
      }
    }
  }
}

/**
 * Implements hook_css_alter().
 */
function edge_css_alter(&$css) {
  global $base_theme_info, $theme_info;

  $overrides = array();

  // Handle .info file properties for base themes.
  if (!empty($base_theme_info)) {
    foreach ($base_theme_info as $name => $base_theme) {
      if (isset($base_theme->info['stylesheets-override'])) {
        $base_theme_path = dirname($base_theme->filename);
        $theme_overrides = _system_info_add_path($base_theme_info[$name]->info['stylesheets-override'], $base_theme_path);
        $overrides = array_merge($overrides, $theme_overrides);
      }
    }
  }
  // Handle .info file properties for active theme.
  if (isset($theme_info->info['stylesheets-override'])) {
    $theme_path = dirname($theme_info->filename);
    $theme_overrides = _system_info_add_path($theme_info->info['stylesheets-override'], $theme_path);
    $overrides = array_merge($overrides, $theme_overrides);
  }

  if (!empty($overrides)) {
    // $css is keyed by filename and may even additionally specify an alternate
    // basename. The theme .info properties are always keyed by basename, so we
    // have to manually compare each file like drupal_get_css() would.
    foreach ($css as $filename => $item) {
      if ($item['type'] == 'file') {
        $basename = (isset($item['basename']) ? $item['basename'] : basename($item['data']));
        if (isset($overrides[$basename])) {
          $css[$filename]['data'] = $overrides[$basename];
        }
      }
    }
  }
}

/**
 * Helper function to determine the value of a table form element.
 *
 * @param $element
 *   The form element whose value is being populated.
 * @param $input
 *   The incoming input to populate the form element. If this is FALSE,
 *   the element's default value should be returned.
 *
 * @return
 *   The data that will appear in the $form_state['values'] collection
 *   for this element. Return nothing to use the default.
 */
function edge_type_table_value($element, $input = FALSE) {
  // If #multiple is FALSE, the regular default value of radio buttons is used.
  if (!empty($element['#multiple'])) {
    // Contrary to #type 'checkboxes', the default value of checkboxes in a
    // table is built from the array keys (instead of array values) of the
    // #default_value property.
    // @todo D8: Remove this weirdness.
    if ($input === FALSE) {
      $element += array('#default_value' => array());
      return drupal_map_assoc(array_keys(array_filter($element['#default_value'])));
    }
    else {
      return is_array($input) ? drupal_map_assoc($input) : array();
    }
  }
}

/**
 * #process callback for #type 'table' to add tableselect support.
 *
 * @see form_process_tableselect()
 * @see theme_tableselect()
 */
function edge_element_process_table($element, &$form_state) {
  if ($element['#tableselect']) {
    if ($element['#multiple']) {
      $value = is_array($element['#value']) ? $element['#value'] : array();
    }
    // Advanced selection behaviour makes no sense for radios.
    else {
      $element['#js_select'] = FALSE;
    }
    // Add a "Select all" checkbox column to the header.
    // @todo D8: Rename into #select_all?
    if ($element['#js_select']) {
      $element['#attached']['js'][] = 'misc/tableselect.js';
      array_unshift($element['#header'], array('class' => array('select-all')));
    }
    // Add an empty header column for radio buttons or when a "Select all"
    // checkbox is not desired.
    else {
      array_unshift($element['#header'], '');
    }

    // @todo FIXME
    if (!isset($element['#default_value']) || $element['#default_value'] === 0) {
      $element['#default_value'] = array();
    }
    // Create a checkbox or radio for each row in a way that the value of the
    // tableselect element behaves as if it had been of #type checkboxes or
    // radios.
    foreach (element_children($element) as $key) {
      // Do not overwrite manually created children.
      if (!isset($element[$key]['select'])) {
        // Determine option label; either an assumed 'title' column, or the
        // first available column containing a #title or #markup.
        // @todo Consider to add an optional $element[$key]['#title_key']
        //   defaulting to 'title'?
        $title = '';
        if (!empty($element[$key]['title']['#title'])) {
          $title = $element[$key]['title']['#title'];
        }
        else {
          // @todo Performance-optimization.
          foreach (element_children($element[$key]) as $column) {
            if (isset($element[$key][$column]['#title'])) {
              $title = $element[$key][$column]['#title'];
              break;
            }
            if (isset($element[$key][$column]['#markup'])) {
              $title = $element[$key][$column]['#markup'];
              break;
            }
          }
        }
        if ($title !== '') {
          $title = t('Update !title', array('!title' => $title));
        }

        // Prepend the select column to existing columns.
        $element[$key] = array('select' => array()) + $element[$key];
        $element[$key]['select'] += array(
          '#type' => $element['#multiple'] ? 'checkbox' : 'radio',
          '#title' => $title,
          '#title_display' => 'invisible',
          // @todo If rows happen to use numeric indexes instead of string keys,
          //   this results in a first row with $key === 0, which is always FALSE.
          '#return_value' => $key,
          '#attributes' => $element['#attributes'],
        );
        $element_parents = array_merge($element['#parents'], array($key));
        if ($element['#multiple']) {
          $element[$key]['select']['#default_value'] = isset($value[$key]) ? $key : NULL;
          $element[$key]['select']['#parents'] = $element_parents;
        }
        else {
          $element[$key]['select']['#default_value'] = ($element['#default_value'] == $key ? $key : NULL);
          $element[$key]['select']['#parents'] = $element['#parents'];
          $element[$key]['select']['#id'] = drupal_html_id('edit-' . implode('-', $element_parents));
        }
      }
    }
  }

  return $element;
}

/**
 * #element_validate callback for #type 'table'.
 */
function edge_element_validate_table($element, &$form_state) {
  $error = FALSE;
  if ($element['#multiple']) {
    if (!is_array($element['#value']) || !count(array_filter($element['#value']))) {
      $error = TRUE;
    }
  }
  elseif (!isset($element['#value']) || $element['#value'] === '') {
    $error = TRUE;
  }
  // @todo Add dependency on #required?
  if ($error) {
    form_error($element, t('No items selected.'));
  }
}

/**
 * #pre_render callback to transform children of an element into #rows suitable for theme_table().
 *
 * This function converts sub-elements of an element of #type 'table' to be
 * suitable for theme_table():
 * - The first level of sub-elements are table rows. Only the #attributes
 *   property is taken into account.
 * - The second level of sub-elements is converted into columns for the
 *   corresponding first-level table row.
 *
 * Simple example usage:
 * @code
 * $form['table'] = array(
 *   '#type' => 'table',
 *   '#header' => array(t('Title'), array('data' => t('Operations'), 'colspan' => '1')),
 *   // Optionally, to add tableDrag support:
 *   '#attached' => array(
 *     'tabledrag' => array(array('order', 'sibling', 'thing-weight')),
 *   ),
 * );
 * foreach ($things as $row => $thing) {
 *   $form['table'][$row]['#weight'] = $thing['weight'];
 *
 *   $form['table'][$row]['title'] = array(
 *     '#type' => 'textfield',
 *     '#default_value' => $thing['title'],
 *   );
 *
 *   // Optionally, to add tableDrag support:
 *   $form['table'][$row]['#attributes'] = array('class' => array('draggable'));
 *   $form['table'][$row]['weight'] = array(
 *     '#type' => 'textfield',
 *     '#title' => t('Weight for @title', array('@title' => $thing['title'])),
 *     '#title_display' => 'invisible',
 *     '#size' => 4,
 *     '#default_value' => $thing['weight'],
 *     '#attributes' => array('class' => array('thing-weight')),
 *   );
 *
 *   // The amount of link columns should be identical to the 'colspan'
 *   // attribute in #header above.
 *   $form['table'][$row]['edit'] = array(
 *     '#type' => 'link',
 *     '#title' => t('Edit'),
 *     '#href' => 'thing/' . $row . '/edit',
 *   );
 * }
 * @endcode
 *
 * @param $element
 *   A structured array containing two sub-levels of elements.
 *
 * @see system_element_info()
 * @see theme_table()
 * @see drupal_process_attached()
 * @see drupal_add_tabledrag()
 */
function edge_pre_render_table($element) {
  foreach (element_children($element) as $first) {
    $row = array('data' => array());
    // Apply attributes of first-level elements as table row attributes.
    if (isset($element[$first]['#attributes'])) {
      $row += $element[$first]['#attributes'];
    }
    // Turn second-level elements into table row columns.
    // @todo Do not render a cell for children of #type 'value'.
    // @see http://drupal.org/node/1248940
    foreach (element_children($element[$first]) as $second) {
      // Assign the element by reference, so any potential changes to the
      // original element are taken over.
      $row['data'][] = array('data' => &$element[$first][$second]);
    }
    $element['#rows'][] = $row;
  }

  // Take over $element['#id'] as HTML ID attribute, if not already set.
  element_set_attributes($element, array('id'));

  // If the custom #attached][tabledrag] is set and there is a HTML ID, convert
  // it into #attached][drupal_add_tabledrag] and inject the HTML ID as first
  // callback argument.
  if (isset($element['#attached']['tabledrag']) && isset($element['#attributes']['id'])) {
    $element['#attached']['drupal_add_tabledrag'] = $element['#attached']['tabledrag'];
    foreach ($element['#attached']['drupal_add_tabledrag'] as &$args) {
      array_unshift($args, $element['#attributes']['id']);
    }
    unset($element['#attached']['tabledrag']);
  }

  return $element;
}

